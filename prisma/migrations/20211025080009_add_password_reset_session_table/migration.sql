-- CreateTable
CREATE TABLE "UserResetSession" (
    "id" TEXT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "userId" TEXT NOT NULL,

    CONSTRAINT "UserResetSession_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "UserResetSession" ADD CONSTRAINT "UserResetSession_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
