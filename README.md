# Edvora Backend Intern Filtering Test

Submitted by: **Juan Miguel A. Mendoza**

**Task**:
_We would like you to create an authentication API with register, login and change password endpoints. The API should be able to handle multiple sessions and terminate specific sessions with a terminate endpoint. On a successful password change, terminate all sessions except the current session. Keep in mind, there is no right answer or time limit for this, so feel free to make whatever you feel is appropriate and/or useful. You are free to use your choice of frameworks._

---

A Postman Documentation of this API is available [here](https://documenter.getpostman.com/view/9245667/UV5f7Ysx)

A Postman Collection of this API is available [here](https://www.getpostman.com/collections/b9f5a56492bf056b1d1f).

---

**Command List:**

```bash
# install dependencies

yarn install
```

```bash
# to run the app locally, create a database reference to your machine first

yarn pull:db
```

```bash
# run the app locally
yarn dev
```

```bash
# check content of the database
yarn view:db
```
