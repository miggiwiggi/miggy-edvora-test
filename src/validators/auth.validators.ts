import Joi from 'joi';

export const register = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(8).required(),
  name: Joi.string().required()
})

export const login = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(8).required()
})

export const resetPassword = Joi.object({
  email: Joi.string().email().required()
})

export const confirmResetPassword = Joi.object({
  requestToken: Joi.string().optional(),
  oldPassword: Joi.string().min(8).optional(),
  newPassword: Joi.string().min(8).required()
});