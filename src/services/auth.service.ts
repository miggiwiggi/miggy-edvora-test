import { NextFunction, Request, Response } from "express";
import { verifyPassword, hashPassword } from "../utils/bcrypt";
import { signJWT } from '../utils/jwt'
import { createError } from '../utils/errors';

import { Users, Sessions, PasswordResetSessions, Prisma, prisma } from "../models";
import { create } from 'domain';

export async function Register(req: Request, res: Response, next: NextFunction) {
  const { name, email, password } = req.body;

  try {
    const existingUser = await Users.findFirst({
      where: {
        email
      }
    });

    if (existingUser) {
      return res.status(400).json(createError(400, 'User with this email already exists!'));
    }

    const hashedPassword = await hashPassword(password);
    const newUser = await Users.create({
      data: {
        name,
        email,
        password: hashedPassword
      }
    });

    const session = await Sessions.create({
      data: {
        userId: newUser.id
      }
    });

    const encodedSessionId = signJWT(session.id)


    return res.status(200).send({
      user: {
        name: newUser?.name,
        email: newUser?.email
      },
      accessToken: encodedSessionId
    })
  } catch (err) {
    console.log(err);
    if (err instanceof Prisma.PrismaClientKnownRequestError) {
      return res.status(500).json(createError(500, err.message));
    }
    return res.status(500).json(createError(500, `INTERNAL SERVER ERROR: ${err}`));
  }
}

export async function Login(req: Request, res: Response, next: NextFunction) {
  const { email, password } = req.body;
  try {
    const user = await Users.findFirst({
      where: {
        email
      }
    })

    if (!user) {
      return res.status(400).json(createError(400, `Invalid Email/Password!`))
    }

    const isPasswordSame = await verifyPassword(password, user.password);
    if (!isPasswordSame) {
      return res.status(400).json(createError(400, 'Invalid Email/Password'))
    }

    const session = await Sessions.create({
      data: {
        userId: user.id
      }
    })

    const encodedSessionId = signJWT(session.id);

    return res.status(200).json({
      user: {
        name: user?.name,
        email: user?.email
      },
      accessToken: encodedSessionId
    })
  } catch (err) {
    console.log(err);
    if (err instanceof Prisma.PrismaClientKnownRequestError) {
      return res.status(500).json(createError(500, err.message));
    }
    return res.status(500).json(createError(500, `INTERNAL SERVER ERROR: ${err}`));
  }
}

export async function Logout(req: Request, res: Response, next: NextFunction) {
  const sessionId = res.locals?.sessionID;
  try {
    await Sessions.delete({
      where: {
        id: sessionId
      }
    });

    return res.status(200).send('ok');
  } catch (err) {
    if (err instanceof Prisma.PrismaClientKnownRequestError) {
      if (err.code === 'P2025') {
        return res.status(400).json(createError(400, 'already logged out'));
      }

      return res.status(400).json(createError(500, err.message));
    }
    return res.status(400).json(createError(500, `INTERNAL SERVER ERROR: ${err}`));
  }
}

export async function RequestPassword(req: Request, res: Response, next: NextFunction) {
  const email = req.query?.email;
  try {
    const user = await Users.findFirst({
      where: {
        email: String(email)
      }
    })

    if (!user) {
      return res.status(400).json(createError(400, 'Email not found'));
    }

    const resetSession = await PasswordResetSessions.create({
      data: {
        userId: user.id
      }
    })

    return res.status(200).send({ requestToken: resetSession.id });
  } catch (err) {
    console.log(err);
    if (err instanceof Prisma.PrismaClientKnownRequestError) {
      return next(createError(500, err.message))
    }
    return res.status(500).json(createError(500, `INTERNAL SERVER ERROR: ${err}`));
  }
}

export async function AuthenticatedResetPassword(req: Request, res: Response, next: NextFunction) {
  const user = res.locals?.user ?? null;
  const sessionID = res.locals?.sessionID as string ?? null;
  const oldPassword = req.body?.oldPassword as string ?? null;
  const newPassword = req.body?.newPassword as string ?? null;

  try {
    if (!user || !sessionID) {
      return res.status(400).json(createError(400, 'Invalid request'));
    }

    if (!oldPassword) {
      return res.status(400).json(createError(400, 'Missing Old Password'));
    }

    const existingUser = await Users.findFirst({
      where: {
        id: user?.id
      }
    })

    // Check if old password is same with the current user's password
    const isOldPasswordSame = await verifyPassword(oldPassword, existingUser?.password as string);
    if (!isOldPasswordSame) {
      return res.status(400).json(createError(400, 'Old Password Mismatch'));
    }

    // change the password
    const hashedPassword = hashPassword(newPassword);
    await Users.update({
      where: {
        id: user?.id
      },
      data: {
        password: hashedPassword,
        updatedAt: new Date()
      }
    });

    // if the user is logged in while resetting the password
    // then delete all other sessions expect the current one
    await Sessions.deleteMany({
      where: {
        NOT: {
          id: sessionID
        }
      }
    })

    return res.status(200).send({ success: true });
  } catch (err) {
    console.log(err);
    if (err instanceof Prisma.PrismaClientKnownRequestError) {
      return res.status(500).json(createError(500, err.message));
    }
    return res.status(500).json(createError(500, `INTERNAL SERVER ERROR: ${err}`));
  }
}

export async function NonAuthenticatedResetPassword(req: Request, res: Response, next: NextFunction) {
  const requestToken = req.body?.requestToken as string ?? null;
  const newPassword = req.body?.newPassword as string ?? null;

  try {
    const resetPasswordDetails = await PasswordResetSessions.findFirst({
      where: {
        id: requestToken
      }
    })

    if (!resetPasswordDetails) {
      return res.status(400).json(createError(400, 'Request not found'));
    }

    const existingUser = await Users.findFirst({
      where: {
        id: resetPasswordDetails.userId
      }
    });

    if (!existingUser) {
      return res.status(400).json(createError(400, 'User not found!'));
    }

    // change the password
    const hashedPassword = hashPassword(String(newPassword));
    await Users.update({
      where: {
        id: resetPasswordDetails.userId
      },
      data: {
        password: hashedPassword,
        updatedAt: new Date()
      }
    });

    await Sessions.deleteMany({
      where: {
        userId: resetPasswordDetails.userId
      }
    });

    await PasswordResetSessions.delete({
      where: {
        id: resetPasswordDetails.id
      }
    });

    return res.status(200).send({ success: true });
  } catch (err) {
    console.log(err);
    if (err instanceof Prisma.PrismaClientKnownRequestError) {
      return res.status(500).json(createError(500, err.message));
    }
    return res.status(500).json(createError(500, `INTERNAL SERVER ERROR: ${err}`));
  }
}