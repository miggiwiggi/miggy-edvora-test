import { PrismaClient, Prisma } from '@prisma/client';
const prisma = new PrismaClient()

export { prisma, Prisma }

export const Users = prisma.user
export const Sessions = prisma.userSession;
export const PasswordResetSessions = prisma.userResetSession;
