import express from 'express'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import morgan from 'morgan'
import cors from 'cors'
import { PORT } from './configs/constants';
import * as Router from './routes';

const app = express();
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(helmet())
app.use(morgan('dev'))
app.use(cors({ origin: true }))

// import routes
app.use(Router.General)
app.use(Router.Auth)

app.listen(PORT, () => {
  console.log('🚀 Server listening on PORT', PORT, '🚀')
})