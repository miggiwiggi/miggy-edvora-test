import { Router } from 'express'
import {
	register,
	login,
	resetPassword,
	confirmResetPassword
} from '../validators/auth.validators';
import validate from '../middlewares/validate';
import auth from '../middlewares/auth';
import * as services from '../services/auth.service';

const router = Router();

router.post('/api/login', validate(login, 'body'), services.Login);
router.post('/api/register', validate(register, 'body'), services.Register)
router.get('/api/logout', auth, services.Logout);
router.get('/api/request-password-reset', validate(resetPassword, 'query'), services.RequestPassword)
router.post('/api/reset-password', validate(confirmResetPassword, 'body'), services.NonAuthenticatedResetPassword)
router.post('/api/reset-password-loggedin', auth, validate(confirmResetPassword, 'body'), services.AuthenticatedResetPassword)

export default router;