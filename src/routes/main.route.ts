import { Router, Request, Response } from 'express'
import auth from '../middlewares/auth';

const router = Router();

router.get('/', (_: Request, res: Response) => {
	return res.send('🚀 API is running! 👏')
})

router.get('/api', (_: Request, res: Response) => {
	return res.send('🚀 API is running! 👏')
})

router.get('/api/me', auth, (_: Request, res: Response) => {
	return res.status(200).send({ user: res.locals.user })
})

export default router;