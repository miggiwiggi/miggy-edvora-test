import bcrypt from 'bcryptjs'
import { PASSWORD_SALT } from '../configs/constants';

export const verifyPassword = async (password: string, hashedPassword: string) => {
  const isSamePassword = bcrypt.compareSync(password, hashedPassword)
  return isSamePassword;
}

export const hashPassword = (password: string) => {
  const salt = bcrypt.genSaltSync(Number(PASSWORD_SALT));
  const result = bcrypt.hashSync(password, salt);
  return result
}