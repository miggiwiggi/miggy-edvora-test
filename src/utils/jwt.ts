
import jwt from 'jsonwebtoken';
import { SESSION_SECRET } from '../configs/constants';

export function signJWT(payload: string) {
  const result = jwt.sign({ sessionId: payload }, String(SESSION_SECRET));
  return result;
}

export function verifyJWT(payload: string) {
  try {
    const decoded = jwt.verify(payload, String(SESSION_SECRET)) as any;
    return decoded.sessionId;
  } catch (err) {
    console.log('ERR WHILE VERIFYING JWT: ', err);
    return false;
  }
}