import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
import utc from 'dayjs/plugin/utc'

dayjs.extend(relativeTime)
dayjs.extend(utc)

import { SESSION_EXPIRY } from '../configs/constants';

export function isSessionExpired(createdTime: string | Date) {
  const expiryDate = dayjs.utc(createdTime).add(Number(SESSION_EXPIRY), 'hour');
  const accessDate = dayjs.utc()
  return accessDate.isAfter(expiryDate)
}