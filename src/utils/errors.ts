export function createError(statusCode: number, message: string) {
  return { code: statusCode, message };
}