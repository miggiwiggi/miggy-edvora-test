import { Request, Response, NextFunction } from 'express'
import { verifyJWT } from '../utils/jwt';
import { isSessionExpired } from '../utils/date';
import { Sessions, Users, prisma } from '../models';

export default async function checkAuth(req: Request, res: Response, next: NextFunction) {
  if (!req.headers.authorization) {
    console.log('Missing Auth Token Header!');
    return res.status(401).send({ message: 'missing authorization header!' });
  }

  const BearerToken = req.headers.authorization;
  const accessToken = BearerToken.split(" ")[1];

  try {

    if (!accessToken) {
      return res.status(401).send({ message: 'missing access token' });
    }

    const decodedId = await verifyJWT(accessToken);

    let session;
    let user;

    session = await Sessions.findFirst({
      where: {
        id: decodedId
      }
    })

    if (!session) {
      return res.status(400).send({ message: 'unauthorized' });
    }

    if (isSessionExpired(session.createdAt)) {
      await Sessions.delete({
        where: {
          id: session.id
        }
      });

      return res.status(400).send({ message: 'expired' });
    }

    user = await Users.findFirst({
      where: {
        id: session.userId
      },
      select: {
        id: true,
        name: true,
        email: true
      }
    });


    if (!user) {
      return res.status(400).send({ message: 'unauthorized' });
    }

    res.locals.sessionID = decodedId;
    res.locals.user = user;

    return next()

  } catch (err) {
    console.log('ERR IN AUTH MIDDLEWARE: ', err);
    return res.status(400).send({ message: 'unauthorized' });
  }
}