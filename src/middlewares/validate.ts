import Joi from 'joi'
import { Request, Response, NextFunction } from 'express'

type IRequestToCheck = 'body' | 'query' | 'params'
export default function validate(schema: Joi.AnySchema, requestToCheck: IRequestToCheck) {
  return (req: Request, res: Response, next: NextFunction) => {

    try {
      schema.validate(req[requestToCheck]);
      return next();
    } catch (err) {
      const error: any = err;
      if (error && error?.details) {
        const message = error?.details?.map((e: any) => e.message).join(',');
        console.log('ERROR in VALIDATE: ', message);
        return res.status(400).send({ message });
      }
      return res.status(400).send(error);
    }
  }
}
